# Cinco de Mayo Speedrun

An API solution to Cinco de Mayo Speedrun labs.
Best way to use those solutions:
* Open a private windows with https://console.cloud.google.com
* Select the username field
* Start the lab
* Copy user/password and validate the security options
* Check the project is correctly selected
* Launch cloudshell and validate the pop-up
* Wait for the cloudshell machine to launch
* Drag and drop the script into the cloud shell interface
* Use `bash <name_of_script>`
* Follow up the output in a separate windows while clicking on validation

# Noticable bugs

* Sometimes the `gcloud set project id <xxx>` command bug and the command after fail
* Sometimes the correct project by default is selected, sometimes not