#!/bin/bash
#project_id=$DEVSHELL_PROJECT_ID
#location="us"
#gcloud config set project $project_id
gcloud beta compute instances create speaking-with-a-webpage --zone=us-central1-f --machine-type=n1-standard-1 --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --scopes=https://www.googleapis.com/auth/cloud-platform --tags=http-server,https-server --image=ubuntu-1604-xenial-v20200429 --image-project=ubuntu-os-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=speaking-with-a-webpage --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any --metadata startup-script='sudo apt-get update; sudo apt-get install -y maven openjdk-8-jdk;'
gcloud compute firewall-rules create default-allow-http --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:80 --source-ranges=0.0.0.0/0 --target-tags=http-server
gcloud compute firewall-rules create default-allow-https --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:443 --source-ranges=0.0.0.0/0 --target-tags=https-server
gcloud compute firewall-rules create dev-ports --allow=tcp:8443 --source-ranges=0.0.0.0/0
# git clone https://github.com/googlecodelabs/speaking-with-a-webpage.git; cd ~/speaking-with-a-webpage/01-hello-https;  mvn clean jetty:run
# cd ~/speaking-with-a-webpage/02-webaudio; mvn clean jetty:run
# cd ~/speaking-with-a-webpage/03-websockets; mvn clean jetty:run