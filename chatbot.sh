project_id=$DEVSHELL_PROJECT_ID
location="us"
gcloud config set project $project_id
bq --location=$location mk \
    --dataset \
    --default_table_expiration 150000 \
    --default_partition_expiration 150000 \
    --description test \
    $project_id:helpdesk

bq load --autodetect --skip_leading_rows=1 --source_format=CSV helpdesk.issues gs://solutions-public-assets/smartenup-helpdesk/ml/issues.csv

bq --location=$location query --nouse_legacy_sql 'CREATE OR REPLACE MODEL `helpdesk.predict_eta_v0` OPTIONS(model_type="linear_reg") AS SELECT  category,  resolutiontime as label FROM   `helpdesk.issues`'
bq --location=$location query --nouse_legacy_sql 'WITH eval_table AS ( SELECT  category,  resolutiontime as label FROM   helpdesk.issues ) SELECT   * FROM   ML.EVALUATE(MODEL helpdesk.predict_eta_v0,     TABLE eval_table)'
bq --location=$location query --nouse_legacy_sql 'CREATE OR REPLACE MODEL `helpdesk.predict_eta` OPTIONS(model_type="linear_reg") AS SELECT  seniority,  experience,  category,  type,  resolutiontime as label FROM   `helpdesk.issues`'
bq --location=$location query --nouse_legacy_sql 'WITH eval_table AS ( SELECT  seniority,  experience,  category,  type,  resolutiontime as label FROM   helpdesk.issues ) SELECT   * FROM   ML.EVALUATE(MODEL helpdesk.predict_eta,     TABLE eval_table)'
